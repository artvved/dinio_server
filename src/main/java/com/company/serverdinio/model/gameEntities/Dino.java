package com.company.serverdinio.model.gameEntities;

import com.company.serverdinio.model.gameEntities.util.BodyElement;
import com.company.serverdinio.model.gameEntities.util.Level;
import com.company.serverdinio.model.gameEntities.physics.MyTransform;

import java.util.List;
import java.util.Objects;


public class Dino {
    protected List<BodyElement> bodyElements;
    protected MyTransform myTransform;
    protected Level level;
    protected double hunger;
    protected double maxHunger;
    protected double hp;
    protected double maxHp;
    protected double speed;
    protected double rotationSpeed;
    protected String name;
    protected int score;

    public List<BodyElement> getBodyElements() {
        return bodyElements;
    }

    public void setBodyElements(List<BodyElement> bodyElements) {
        this.bodyElements = bodyElements;
    }

    public MyTransform getMyTransform() {
        return myTransform;
    }

    public void setMyTransform(MyTransform myTransform) {
        this.myTransform = myTransform;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public double getHunger() {
        return hunger;
    }

    public void setHunger(double hunger) {
        this.hunger = hunger;
    }

    public double getMaxHunger() {
        return maxHunger;
    }

    public void setMaxHunger(double maxHunger) {
        this.maxHunger = maxHunger;
    }

    public double getHp() {
        return hp;
    }

    public void setHp(double hp) {
        this.hp = hp;
    }

    public double getMaxHp() {
        return maxHp;
    }

    public void setMaxHp(double maxHp) {
        this.maxHp = maxHp;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getRotationSpeed() {
        return rotationSpeed;
    }

    public void setRotationSpeed(double rotationSpeed) {
        this.rotationSpeed = rotationSpeed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Dino{" +
                "bodyElements=" + bodyElements +
                ", myTransform=" + myTransform +
                ", level=" + level +
                ", hunger=" + hunger +
                ", maxHunger=" + maxHunger +
                ", hp=" + hp +
                ", maxHp=" + maxHp +
                ", speed=" + speed +
                ", rotationSpeed=" + rotationSpeed +
                ", name='" + name + '\'' +
                ", score=" + score +
                '}';
    }
}
