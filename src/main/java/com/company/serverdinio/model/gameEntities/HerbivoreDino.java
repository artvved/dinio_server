package com.company.serverdinio.model.gameEntities;

import com.company.serverdinio.model.gameEntities.physics.MyBoxCollider2D;
import com.company.serverdinio.model.gameEntities.physics.MyTransform;
import com.company.serverdinio.model.gameEntities.util.BodyElement;
import com.company.serverdinio.model.gameEntities.util.ConsumeBodyElement;
import com.company.serverdinio.model.gameEntities.util.Level;

import java.util.ArrayList;
import java.util.List;

public class HerbivoreDino extends Dino {

    public HerbivoreDino() {
        bodyElements = new ArrayList<>();
        BodyElement body = new BodyElement(
                new MyBoxCollider2D(
                        new MyTransform(0, 0, 0, 10, 20)));
        BodyElement head = new ConsumeBodyElement(
                new MyBoxCollider2D(
                        new MyTransform(5, 13, 0, 6, 6)));

        bodyElements.add(body);
        bodyElements.add(head);
        //x y where to spawn can be given as params
        myTransform = new MyTransform(0, 0, 0, 1, 1);
        level = new Level();
        hunger = maxHunger = 100;
        hp = maxHp = 100;
        speed = 2;
        rotationSpeed = 2;


    }



    @Override
    public String toString() {
        return "HerbivoreDino{" +
                "bodyElements=" + bodyElements +
                ", myTransform=" + myTransform +
                ", level=" + level +
                ", hunger=" + hunger +
                ", maxHunger=" + maxHunger +
                ", hp=" + hp +
                ", maxHp=" + maxHp +
                ", speed=" + speed +
                ", rotationSpeed=" + rotationSpeed +
                '}';
    }
}
