package com.company.serverdinio.dto;

import lombok.Data;

@Data
public class UserDto {
    private String name;
    private String device_id;
}
