package com.company.serverdinio.model.gameEntities;

import com.company.serverdinio.model.gameEntities.physics.MyTransform;

public class Grass extends Food{
    public Grass(MyTransform myTransform, double value) {
        super(myTransform, value);
    }

    public Grass() {
    }

    @Override
    public String toString() {
        return "Grass{" +
                "id=" + id +
                ", myTransform=" + myTransform +
                ", value=" + value +
                '}';
    }
}
