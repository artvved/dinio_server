package com.company.serverdinio.model.gameEntities;

import com.company.serverdinio.model.gameEntities.physics.MyBoxCollider2D;
import com.company.serverdinio.model.gameEntities.physics.MyTransform;
import com.company.serverdinio.model.gameEntities.util.BodyElement;
import com.company.serverdinio.model.gameEntities.util.ConsumeBodyElement;
import com.company.serverdinio.model.gameEntities.util.Level;

import java.util.ArrayList;
import java.util.List;

public class PredatorDino extends Dino {
    public PredatorDino() {
        bodyElements = new ArrayList<>();
        BodyElement body = new BodyElement(
                new MyBoxCollider2D(
                        new MyTransform(0, 0, 0, 10, 20)));
        BodyElement head = new ConsumeBodyElement(
                new MyBoxCollider2D(
                        new MyTransform(5, 13, 0, 6, 6)));

        bodyElements.add(body);
        bodyElements.add(head);
        //x y where to spawn can be given as params
        myTransform = new MyTransform(0, 0, 0, 1, 1);
        level = new Level();
        hunger = maxHunger = 100;
        hp = maxHp = 100;
        speed = 1.5;
        rotationSpeed = 1.5;


    }



    @Override
    public String toString() {
        return "PredatorDino{" +
                "bodyElements=" + bodyElements +
                ", myTransform=" + myTransform +
                ", level=" + level +
                ", hunger=" + hunger +
                ", maxHunger=" + maxHunger +
                ", hp=" + hp +
                ", maxHp=" + maxHp +
                ", speed=" + speed +
                ", rotationSpeed=" + rotationSpeed +
                '}';
    }
}
