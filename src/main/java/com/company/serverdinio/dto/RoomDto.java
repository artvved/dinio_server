package com.company.serverdinio.dto;

import lombok.Data;


//это dto нужно только чтобы пользователь видел комнату в списке комнат. поэтому у нее нет состояния - его не надо передавать
//в этот список
@Data
public class RoomDto {
    private String name;
    private int maxCountOfPlayers;
    private int currentCountOfPlayers;
    private boolean isPrivate;

    public RoomDto(String name, int maxCountOfPlayers, int currentCountOfPlayers, boolean isPrivate) {
        this.name = name;
        this.maxCountOfPlayers = maxCountOfPlayers;
        this.currentCountOfPlayers = currentCountOfPlayers;
        this.isPrivate = isPrivate;
    }
}