package com.company.serverdinio.model.gameEntities;

import com.company.serverdinio.model.gameEntities.physics.MyTransform;

public class Meat extends Food{
    public Meat(MyTransform myTransform, double value) {
        super(myTransform, value);
    }

    public Meat() {
    }

    @Override
    public String toString() {
        return "Meat{" +
                "id=" + id +
                ", myTransform=" + myTransform +
                ", value=" + value +
                '}';
    }
}
