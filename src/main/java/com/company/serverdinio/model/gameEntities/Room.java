package com.company.serverdinio.model.gameEntities;
import com.company.serverdinio.exception.WebException;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Objects;

@Data
public class Room {

    private String name;
    private int maxCountOfPlayers;
    private int currentCountOfPlayers;
    private boolean isPrivate;
    private GameState gameState;

    public Room(String name, int maxCountOfPlayers, boolean isPrivate){
        this.name = name;
        this.currentCountOfPlayers = 0;
        this.maxCountOfPlayers = maxCountOfPlayers;
        this.isPrivate = isPrivate;
        this.gameState = new GameState();
    }

    public Room(String name){
        this.name = name;
        this.currentCountOfPlayers = 0;
        this.maxCountOfPlayers = 10;
        this.isPrivate = true;
        this.gameState = new GameState();
    }

    public void incCurrentCountOfPlayers() {
        if (currentCountOfPlayers+1>maxCountOfPlayers){
            throw new WebException("Room is full", HttpStatus.NOT_FOUND);
        }
        currentCountOfPlayers++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return name.equals(room.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Room{" +
                "name='" + name + '\'' +
                ", maxCountOfPlayers=" + maxCountOfPlayers +
                ", currentCountOfPlayers=" + currentCountOfPlayers +
                ", isPrivate=" + isPrivate +
                ", gameState=" + gameState +
                '}';
    }
}