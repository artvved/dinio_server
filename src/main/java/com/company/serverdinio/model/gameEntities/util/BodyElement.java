package com.company.serverdinio.model.gameEntities.util;

import com.company.serverdinio.model.gameEntities.physics.MyBoxCollider2D;

public class BodyElement {
    protected MyBoxCollider2D myBoxCollider2D;

    public BodyElement() {
    }

    public BodyElement(MyBoxCollider2D myBoxCollider2D) {
        this.myBoxCollider2D = myBoxCollider2D;
    }

    public MyBoxCollider2D getMyBoxCollider2D() {
        return myBoxCollider2D;
    }

    public void setMyBoxCollider2D(MyBoxCollider2D myBoxCollider2D) {
        this.myBoxCollider2D = myBoxCollider2D;
    }

    @Override
    public String toString() {
        return "BodyElement{" +
                "myBoxCollider2D=" + myBoxCollider2D +
                '}';
    }
}
