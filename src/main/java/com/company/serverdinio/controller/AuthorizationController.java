package com.company.serverdinio.controller;

import com.company.serverdinio.dao.UserDao;
import com.company.serverdinio.dto.UserDto;
import com.company.serverdinio.exception.WebException;
import com.company.serverdinio.model.Message;
import com.company.serverdinio.service.AuthorizationService;
import com.company.serverdinio.model.dbEntities.User;
import com.company.serverdinio.model.gameEntities.Dino;
import com.company.serverdinio.model.gameEntities.HerbivoreDino;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthorizationController {
    private final AuthorizationService authorizationService;

    @PostMapping("/reg")
    public void reg (@RequestBody UserDto userDto) throws Exception {
        authorizationService.regUser(userDto.getName(), userDto.getDevice_id());
    }

    @PostMapping("/")
    public UserDto auth (@RequestBody String deviceId) throws Exception {
        User user = authorizationService.checkDevice(deviceId);
        UserDto userdto = new UserDto();
        userdto.setDevice_id(user.getDevice());
        userdto.setName(user.getName());
        return userdto;
    }

    @PostMapping("/ch")
    public void changeName (@RequestBody UserDto userDto) throws Exception {
        if (!authorizationService.changeName(userDto.getName(), userDto.getDevice_id())){
            throw new WebException("Name already in use", HttpStatus.BAD_REQUEST);
        };
    }

}
