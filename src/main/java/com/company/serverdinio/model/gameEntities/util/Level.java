package com.company.serverdinio.model.gameEntities.util;

import java.util.Arrays;

public class Level {
    private int currentLevel;
    private int currentExp;
    private int currentExpToNewLevel;

    private final int[] expToNewLevel;

    public Level() {
        expToNewLevel = new int[10];
        for (int i : expToNewLevel) {
            expToNewLevel[i]=5*(i);
        }
        currentLevel=0;
        currentExp=0;
        currentExpToNewLevel= expToNewLevel[1];
    }

    public void recalculateExp(int exp){
        if (currentLevel==10)
            return;

        currentExp+=exp;
        while (currentExp>=currentExpToNewLevel){
            currentExp-=currentExpToNewLevel;
            currentLevel++;
            currentExpToNewLevel= expToNewLevel[currentLevel+1];
        }
    }

    public int getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(int currentLevel) {
        this.currentLevel = currentLevel;
    }

    public int getCurrentExp() {
        return currentExp;
    }

    public void setCurrentExp(int currentExp) {
        this.currentExp = currentExp;
    }

    public int getCurrentExpToNewLevel() {
        return currentExpToNewLevel;
    }

    public void setCurrentExpToNewLevel(int currentExpToNewLevel) {
        this.currentExpToNewLevel = currentExpToNewLevel;
    }

    public int[] getExpToNewLevel() {
        return expToNewLevel;
    }

    @Override
    public String toString() {
        return "Level{" +
                "currentLevel=" + currentLevel +
                ", currentExp=" + currentExp +
                ", currentExpToNewLevel=" + currentExpToNewLevel +
                ", expToNewLevel=" + Arrays.toString(expToNewLevel) +
                '}';
    }
}
