package com.company.serverdinio.model.gameEntities.util;

import com.company.serverdinio.model.gameEntities.physics.MyBoxCollider2D;

public class ConsumeBodyElement extends BodyElement{
    public ConsumeBodyElement(MyBoxCollider2D myBoxCollider2D) {
        super(myBoxCollider2D);
    }

    @Override
    public String toString() {
        return "ConsumeBodyElement{" +
                "myBoxCollider2D=" + myBoxCollider2D +
                '}';
    }
}
