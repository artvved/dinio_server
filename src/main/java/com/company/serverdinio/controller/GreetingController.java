package com.company.serverdinio.controller;

import com.company.serverdinio.config.RuntimeTypeAdapterFactory;
import com.company.serverdinio.exception.WebException;
import com.company.serverdinio.model.Message;
import com.company.serverdinio.model.gameEntities.*;
import com.company.serverdinio.model.gameEntities.physics.MyBoxCollider2D;
import com.company.serverdinio.model.gameEntities.physics.MyTransform;
import com.company.serverdinio.model.gameEntities.util.AttackBodyElement;
import com.company.serverdinio.model.gameEntities.util.BodyElement;
import com.company.serverdinio.model.gameEntities.util.ConsumeBodyElement;
import com.company.serverdinio.service.GameLogic;
import com.company.serverdinio.service.RoomService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import liquibase.pro.packaged.T;
import nonapi.io.github.classgraph.json.JSONUtils;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.HtmlUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

@RestController

public class GreetingController {

    //массив с комнатами
    private GameLogic gameLogic=new GameLogic();
    private Gson gson;
    RoomService roomService = new RoomService();
    GameState gameState=new GameState();


    public GreetingController() {

        //default constructor with json opts
        RuntimeTypeAdapterFactory<BodyElement> elAdapterFactory
                = RuntimeTypeAdapterFactory.of(BodyElement.class, "type");
        elAdapterFactory.registerSubtype(AttackBodyElement.class, "AttackBodyElement, Assembly-CSharp");
        elAdapterFactory.registerSubtype(ConsumeBodyElement.class, "ConsumeBodyElement, Assembly-CSharp");
        elAdapterFactory.registerSubtype(BodyElement.class, "BodyElement, Assembly-CSharp");

        RuntimeTypeAdapterFactory<Dino> dinoAdapterFactory
                = RuntimeTypeAdapterFactory.of(Dino.class, "type");
        dinoAdapterFactory.registerSubtype(HerbivoreDino.class, "HerbivoreDino, Assembly-CSharp");
        dinoAdapterFactory.registerSubtype(PredatorDino.class, "PredatorDino, Assembly-CSharp");

        RuntimeTypeAdapterFactory<GameState> gameAdapterFactory
                = RuntimeTypeAdapterFactory.of(GameState.class, "type");
        gameAdapterFactory.registerSubtype(GameState.class, "GameState, Assembly-CSharp");

        RuntimeTypeAdapterFactory<Food> foodAdapterFactory
                = RuntimeTypeAdapterFactory.of(Food.class, "type");
        foodAdapterFactory.registerSubtype(Grass.class, "Grass, Assembly-CSharp");
        foodAdapterFactory.registerSubtype(Meat.class, "Meat, Assembly-CSharp");

        gson = new GsonBuilder()
                .registerTypeAdapterFactory(elAdapterFactory)
                .registerTypeAdapterFactory(dinoAdapterFactory)
                .registerTypeAdapterFactory(gameAdapterFactory)
                .registerTypeAdapterFactory(foodAdapterFactory)
                .create();

    }


    @MessageMapping("/{room_id}")
    @SendTo("/topic/{room_id}")
    //@MessageMapping("/hello")
    //@SendTo("/topic/greetings")
    public String game(Message message) throws Exception {
        Logger.getLogger(GreetingController.class.getName()).info(message.getName());




        String state = message.getName();
        boolean hasChanges;
        if (state.charAt(0) == '1') {
            hasChanges = true;
        } else {
            hasChanges = false;
        }
        state = state.substring(1);
        StringBuilder sb=new StringBuilder();
        for (int i=0;i<state.length();i++){
            if (state.charAt(i)=='{'){
                state = state.substring(i);
                break;
            }
           sb.append(state.charAt(i));
        }
        String room_id = sb.toString();

        System.out.println(state);
        System.out.println(room_id);
        if (room_id.equals(""))
            System.out.println("empty");;


        Room room=roomService.getByName(room_id);
        GameState gameState=room.getGameState();
        //System.out.println("GOT " + message.getName());

        GameState newState = gson.fromJson(state, GameState.class);


        if (hasChanges) {
            gameState = newState;
        }
        //do logic
        gameLogic.doLogic(gameState);



        roomService.setByName(room_id,gameState);

        String json = gson.toJson(gameState, GameState.class);

        //System.out.println("json " +json);
        System.out.println();


        return json;

    }


}

