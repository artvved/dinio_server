package com.company.serverdinio.service;

import com.company.serverdinio.dao.UserDao;
import com.company.serverdinio.exception.WebException;
import com.company.serverdinio.model.dbEntities.User;
import com.company.serverdinio.model.gameEntities.Dino;
import com.company.serverdinio.model.gameEntities.GameState;
import com.company.serverdinio.model.gameEntities.Room;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.LinkedList;

@Service
@RequiredArgsConstructor
public class RoomService {
    public static LinkedList<Room> rooms = new LinkedList<>();

    public boolean createRoom(Room room) {
        if (rooms.contains(room)) {
            return false;
        } else {
            rooms.add(room);
            return true;
        }
    }

    public Room random() throws Exception {
        if (rooms.isEmpty()) {
            throw new WebException("No rooms", HttpStatus.NOT_FOUND);
        }
        LinkedList<Room> openRooms = new LinkedList<>();
        for (Room r : rooms
        ) {
            if (!r.isPrivate()) {
                openRooms.add(r);
            }
        }
        if (openRooms.isEmpty()) {
            throw new WebException("No rooms", HttpStatus.NOT_FOUND);
        }
        Room maxPlayersCount = openRooms.get(0);
        for (Room r : openRooms
        ) {
            if (r.getCurrentCountOfPlayers() > maxPlayersCount.getCurrentCountOfPlayers()) {
                maxPlayersCount = r;
            }
        }
        return maxPlayersCount;
    }

    public Room getByName(String name) throws Exception {
        for (Room r : rooms
        ) {

            if (r.getName().equals(name)) {

                return r;
            }
        }
        throw new WebException("Wrong name", HttpStatus.BAD_REQUEST);
    }

    public void setByName(String name, GameState state) throws Exception {
        for (Room r : rooms
        ) {

            if (r.getName().equals(name)) {

                r.setGameState(state);
                return;
            }
        }
        throw new WebException("Wrong name", HttpStatus.BAD_REQUEST);
    }


    public void deleteByName(String name) {
        for (Iterator<Room> it = rooms.iterator(); it.hasNext(); ) {
            if (it.next().getName().equals(name)) {
                it.remove();
                break;
            }
        }
        throw new WebException("Wrong name", HttpStatus.BAD_REQUEST);
    }

    public void addDinoInRoom(Dino dino, Room room) {
        room.getGameState().addDino(dino);
        room.incCurrentCountOfPlayers();
    }

    public Room findRoomByName(String name) {
        if (rooms.contains(new Room(name))) {
            for (Room r : rooms
            ) {
                if (r.getName().equals(name)) {
                    return r;
                }
            }
        }
        throw new WebException("No room with this name", HttpStatus.NOT_FOUND);
    }

}
