package com.company.serverdinio.model.gameEntities;

import com.company.serverdinio.model.gameEntities.physics.MyBoxCollider2D;
import com.company.serverdinio.model.gameEntities.physics.MyTransform;

public class Food {
    protected int id;
    protected MyTransform myTransform;
    protected double value;

    public Food() {
    }

    public Food(MyTransform myTransform, double value) {
        this.myTransform = myTransform;
        this.value = value;
    }


    public MyTransform getMyTransform() {
        return myTransform;
    }

    public void setMyTransform(MyTransform myTransform) {
        this.myTransform = myTransform;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Food{" +
                "id=" + id +
                ", myTransform=" + myTransform +
                ", value=" + value +
                '}';
    }
}
