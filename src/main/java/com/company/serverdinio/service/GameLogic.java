package com.company.serverdinio.service;

import com.company.serverdinio.model.gameEntities.Dino;
import com.company.serverdinio.model.gameEntities.Food;
import com.company.serverdinio.model.gameEntities.GameState;
import com.company.serverdinio.model.gameEntities.physics.MyTransform;
import com.company.serverdinio.model.gameEntities.util.AttackBodyElement;
import com.company.serverdinio.model.gameEntities.util.BodyElement;
import com.company.serverdinio.model.gameEntities.util.ConsumeBodyElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class GameLogic {
    private Timer timer = new Timer();
    private int i = 0;
    private FoodSpawner foodSpawner = new FoodSpawner();
    private final double HUNGER_DEC_COEF = 1;
    private final double COLLISION_COEF = 0;

    public GameLogic() {
        TimerTask t = new TimerTask() {
            @Override
            public void run() {
                i++;
                System.out.println(i);
            }
        };
        timer.schedule(t, 0, 1000);
    }


    public void doLogic(GameState state) {

        //посотреть позицию всех динозавтров
        //если касаются attackElements с другими или consumeelements с едой - > поменять хп експ голод, кикнуть если 0

        List<Dino> dinos = state.getDinos();
        List<Food> foods = state.getFoodList();

        if (i > 0) {
            validateDinosHP(dinos);
            decreaseHunger(dinos);
            i = 0;
        }
        foodSpawn(foods);

        for (int i = 0; i < dinos.size(); i++) {
            for (Food food : foods)
                if (colisionWithFood(dinos.get(i), food))
                {
                    dinos.get(i).setHunger(dinos.get(i).getHunger()+5);
                }
            for (int j = i + 1; j < dinos.size(); j++) {
                if (colisionWithDinos(dinos.get(i), dinos.get(j))) {
                    //dinos.get(i) - кусает
                    dinos.get(i).setScore(dinos.get(i).getScore()+1);
                    dinos.get(j).setHp(dinos.get(j).getHp()-1);
                }
                if (colisionWithDinos(dinos.get(j), dinos.get(i))) {
                    //dinos.get(j) - кусает
                    dinos.get(j).setScore(dinos.get(j).getScore()+1);
                    dinos.get(i).setHp(dinos.get(i).getHp()-1);
                }
            }
        }
    }




    private boolean colisionWithFood(Dino dino, Food food)
    {
        ArrayList<Line> dinos_jaws = getDinosJaws(dino);
        ArrayList<Line> food_lines = getFoodLines(food);

        for (Line line1 : dinos_jaws)
            for (Line line2 : food_lines)
                if (intersect(line1.point1, line1.point2, line2.point1, line2.point2, COLLISION_COEF)) return true;
        return false;
    }

    private boolean colisionWithDinos(Dino dino1, Dino dino2)
    {
        //ArrayList<Line> dino1s_lines = getDinosLines(dino1);
        ArrayList<Line> dino2s_lines = getDinosLines(dino2);
        ArrayList<Line> dino1s_jaws = getDinosJaws(dino1);
        //ArrayList<Line> dino2s_jaws = getDinosJaws(dino2);

        for (Line line1 : dino1s_jaws)
            for (Line line2 : dino2s_lines)
                if (intersect(line1.point1, line1.point2, line2.point1, line2.point2, COLLISION_COEF)) return true;
        return false;
    }

    private ArrayList<Line> getDinosLines(Dino dino) {
        ArrayList<Line> dinos_lines = new ArrayList<Line>();
        double x, y;
        Line line;
        Point point1, point2, point3, point4;
        MyTransform transDino = dino.getMyTransform();

        double scale = transDino.getScaleX()*1;

        for (BodyElement elem : dino.getBodyElements()) {
            MyTransform transElem = elem.getMyBoxCollider2D().getMyTransform();
            x = transDino.getX() + transElem.getX();
            y = transDino.getY() + transElem.getY();

            point1 = rotatePoint(x, y, x - (transElem.getScaleX() / 2) * scale, y - (transElem.getScaleY() / 2) * scale, (transDino.getRotation() * Math.PI) / 180);
            point2 = rotatePoint(x, y, x - (transElem.getScaleX() / 2) * scale, y + (transElem.getScaleY() / 2) * scale, (transDino.getRotation() * Math.PI) / 180);
            point3 = rotatePoint(x, y, x + (transElem.getScaleX() / 2) * scale, y + (transElem.getScaleY() / 2) * scale, (transDino.getRotation() * Math.PI) / 180);
            point4 = rotatePoint(x, y, x + (transElem.getScaleX() / 2) * scale, y - (transElem.getScaleY() / 2) * scale, (transDino.getRotation() * Math.PI) / 180);
            dinos_lines.add(new Line(point1, point2));
            dinos_lines.add(new Line(point2, point3));
            dinos_lines.add(new Line(point3, point4));
            dinos_lines.add(new Line(point1, point4));
        }

        return dinos_lines;
    }

    private ArrayList<Line> getFoodLines(Food food)
    {
        ArrayList<Line> lines = new ArrayList<>();
        MyTransform foodTrans = food.getMyTransform();

        double x = foodTrans.getX();
        double y = foodTrans.getY();

        Point point1 = rotatePoint(x, y, x - foodTrans.getScaleX() / 2, y - foodTrans.getScaleY() / 2, (foodTrans.getRotation() * Math.PI) / 180);
        Point point2 = rotatePoint(x, y, x - foodTrans.getScaleX() / 2, y + foodTrans.getScaleY() / 2, (foodTrans.getRotation() * Math.PI) / 180);
        Point point3 = rotatePoint(x, y, x + foodTrans.getScaleX() / 2, y + foodTrans.getScaleY() / 2, (foodTrans.getRotation() * Math.PI) / 180);
        Point point4 = rotatePoint(x, y, x + foodTrans.getScaleX() / 2, y - foodTrans.getScaleY() / 2, (foodTrans.getRotation() * Math.PI) / 180);

        lines.add(new Line(point1, point2));
        lines.add(new Line(point2, point3));
        lines.add(new Line(point3, point4));
        lines.add(new Line(point1, point4));

        return lines;
    }

    //Вычисление ориентированной площади треугольника
    private double calcArea(Point a, Point b, Point c, double eps) {
        return sign((b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x), eps);
    }
    private int sign(double x, double eps)
    {
        if (x <= eps && x >= -eps) return 0;
        else if (x > eps) return 1;
        else return -1;
    }

    //Проверка на то, проекции отрезков пересекаются - необходимо для случая, когда они лежат на одной прямой, ведь тогда площади будут равны 0.
    private boolean intersect_all(double a, double b, double c, double d) {
        double x;
        if (a > b) {
            x = a;
            a = b;
            b = x;
        }
        if (c > d) {
            x = c;
            c = d;
            d = x;
        }

        return Math.max(a, c) <= Math.min(b, d);
    }

    //Основная проверка на пересечение отрезков
    private boolean intersect(Point a, Point b, Point c, Point d, double eps) {

        //Положительный результат вернется тогда, когда площади первых двух и вторых двух треугольников будут иметь разные знаки между собой,
        //т.е. S(ABC) * S(ABD) < 0 и S(CDA) * S(CDB) < 0;
        //или же когда, два отрезка будут лежать на одной прямой - тогда все площади равны нулю и здесь играет роль первая проверка на пересечение проекций.
        return intersect_all(a.x, b.x, c.x, d.x) && intersect_all(a.y, b.y, c.y, d.y) // проверка на то, что отрезки лежат друг в друге
                && calcArea(a, b, c, eps) * calcArea(a, b, d, eps) <= 0  //проверка знака площади первых треугольников, образованных на основании первого отрезка AB
                //и двух точек второго отрезка С и D
                && calcArea(c, d, a, eps) * calcArea(c, d, b, eps) <= 0; //аналогичная проверка знака площади вторых треугольников, образованных на основании второго отрезка CD
        //и двух точек первого отрезка A и B
    }

    private class Line {
        public Point point1, point2;

        public Line(Point point1, Point point2) {
            this.point1 = point1;
            this.point2 = point2;
        }

        public Line() {
            point1 = new Point();
            point2 = new Point();
        }
    }

    private class Point {
        public double x, y;

        public Point(double x, double y) {
            this.x = x;
            this.y = y;
        }

        public Point() {
            x = 0;
            y = 0;
        }
    }

    private Point rotatePoint(double x0, double y0, double x1, double y1, double a) {
        Point point = new Point();
        point.x = x0 + (x1 - x0) * Math.cos(a) - (y1 - y0) * Math.sin(a);
        point.y = y0 + (x1 - x0) * Math.sin(a) + (y1 - y0) * Math.cos(a);
        return point;
    }
    private ArrayList<Line> getDinosJaws(Dino dino)
    {
        ArrayList<Line> dinos_jaws = new ArrayList<>();
        double x, y;
        Point point1, point2, point3, point4;
        MyTransform transDino = dino.getMyTransform();
        double scale = transDino.getScaleX() * 4;
        for (BodyElement elem : dino.getBodyElements())
        {
            if (elem instanceof AttackBodyElement || elem instanceof ConsumeBodyElement)
            {
                MyTransform transElem = elem.getMyBoxCollider2D().getMyTransform();
                x = transDino.getX() + transElem.getX();
                y = transDino.getY() + transElem.getY();

                point1 = rotatePoint(x, y, x - (transElem.getScaleX() / 2) * scale, y - (transElem.getScaleY() / 2) * scale, (transDino.getRotation() * Math.PI) / 180);
                point2 = rotatePoint(x, y, x - (transElem.getScaleX() / 2) * scale, y + (transElem.getScaleY() / 2) * scale, (transDino.getRotation() * Math.PI) / 180);
                point3 = rotatePoint(x, y, x + (transElem.getScaleX() / 2) * scale, y + (transElem.getScaleY() / 2) * scale, (transDino.getRotation() * Math.PI) / 180);
                point4 = rotatePoint(x, y, x + (transElem.getScaleX() / 2) * scale, y - (transElem.getScaleY() / 2) * scale, (transDino.getRotation() * Math.PI) / 180);

                dinos_jaws.add(new Line(point1, point2));
                dinos_jaws.add(new Line(point2, point3));
                dinos_jaws.add(new Line(point3, point4));
                dinos_jaws.add(new Line(point1, point4));

                return dinos_jaws;
            }
        }
        return dinos_jaws;
    }

    private void foodSpawn(List<Food> foods) {
        if (foods.size() < 15) {
            foods.add(foodSpawner.spawnRandomGrass());
        }

    }
    private void validateDinosHP(List<Dino> dinos) {
        for (int i=0;i<dinos.size();i++) {
            Dino d =dinos.get(i);
            //d.setHp(d.getHp() - 10);
           /* if (d.getHp()<=0){
                dinos.remove(d);
                i--;
            }*/
        }

    }
    private void decreaseHunger(List<Dino> dinos) {
        for (Dino d : dinos) {
            d.setHunger(d.getHunger() - HUNGER_DEC_COEF);
        }

    }
}
