package com.company.serverdinio.controller;
import com.company.serverdinio.dto.RoomDto;
import com.company.serverdinio.dto.UserDto;
import com.company.serverdinio.exception.WebException;
import com.company.serverdinio.model.gameEntities.Room;
import com.company.serverdinio.service.RoomService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


import java.util.LinkedList;
import java.util.logging.Logger;

@RestController
@RequestMapping("/room")
@RequiredArgsConstructor
public class RoomController {
    private final RoomService roomService;

    @PostMapping("/create")
    public void createRoom (@RequestBody RoomDto roomDto) {
        System.out.println(roomDto.toString());
        Room room = new Room(roomDto.getName(), roomDto.getMaxCountOfPlayers(), roomDto.isPrivate());
        if(roomService.createRoom(room)){
            System.out.println("массив комнат: " + RoomService.rooms);
        }
        else{
            throw new WebException("Room with this name already exists", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/random")
    public RoomDto getRandomRoom () throws Exception {
        Room room = roomService.random();
        System.out.println(room.toString());
        return new RoomDto(room.getName(), room.getMaxCountOfPlayers(), room.getCurrentCountOfPlayers(), room.isPrivate());
    }

    @PostMapping("/get")
    public RoomDto getRoomByName (@RequestBody String name) throws Exception {
        Logger.getLogger(RoomController.class.getName()).info(name);
        Room room = roomService.getByName(name);
        System.out.println(room.toString());
        return new RoomDto(room.getName(), room.getMaxCountOfPlayers(), room.getCurrentCountOfPlayers(), room.isPrivate());
    }

    @PostMapping("/del")
    public void delete (@RequestBody String name) throws Exception {
        roomService.deleteByName(name);
    }

    @GetMapping("/getAll")
    public LinkedList<RoomDto> getAllRooms () throws Exception {
        LinkedList<RoomDto> roomsDto = new LinkedList<>();
        for (Room r: RoomService.rooms
             ) {
            roomsDto.add(new RoomDto(r.getName(), r.getMaxCountOfPlayers(), r.getCurrentCountOfPlayers(), r.isPrivate()));
        }
        return roomsDto;
    }
}