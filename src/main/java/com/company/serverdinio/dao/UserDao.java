package com.company.serverdinio.dao;

import com.company.serverdinio.model.dbEntities.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserDao extends CrudRepository<User, Integer> {
    boolean existsByDevice(String device);
    boolean existsByName(String name);
    //User getByDevice(String device);

    User getAllByDevice(String device);

}
