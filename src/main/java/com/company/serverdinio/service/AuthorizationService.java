package com.company.serverdinio.service;

import com.company.serverdinio.dao.UserDao;
import com.company.serverdinio.exception.WebException;
import com.company.serverdinio.model.dbEntities.User;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthorizationService {
    private final UserDao userDao;

    public void regUser (String name, String device_id) throws Exception {
        if (name!=null && name.length()>0) {
            User user = checkDevice(device_id);
            user.setName(name);
            userDao.save(user);
        }
        else {
            throw new WebException("Поле имя пустое", HttpStatus.BAD_REQUEST);
    }
    }

    public User checkDevice (String deviceId) {
        if (userDao.existsByDevice(deviceId)){
            return userDao.getAllByDevice(deviceId);
        }
        else{
            User user = new User();
            user.setDevice(deviceId);
            userDao.save(user);
            return user;
        }

    }

    public boolean changeName(String name, String device_id){
        if (userDao.existsByDevice(device_id) && !userDao.existsByName(name)){
            User user = checkDevice(device_id);
            user.setName(name);
            userDao.save(user);
            return true;
        }
        else{
            return false;
        }
    }
}
