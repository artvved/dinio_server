package com.company.serverdinio.model.gameEntities.physics;

public class MyBoxCollider2D {
    private MyTransform myTransform;

    public MyBoxCollider2D() {
    }

    public MyBoxCollider2D(MyTransform myTransform) {
        this.myTransform = myTransform;
    }

    public MyTransform getMyTransform() {
        return myTransform;
    }

    public void setMyTransform(MyTransform myTransform) {
        this.myTransform = myTransform;
    }

    @Override
    public String toString() {
        return "MyBoxCollider2D{" +
                "myTransform=" + myTransform +
                '}';
    }
}
