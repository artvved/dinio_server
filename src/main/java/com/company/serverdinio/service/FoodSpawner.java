package com.company.serverdinio.service;

import com.company.serverdinio.model.gameEntities.Food;
import com.company.serverdinio.model.gameEntities.Grass;
import com.company.serverdinio.model.gameEntities.physics.MyTransform;

import java.util.Random;

public class FoodSpawner {
    private  int foodId=0;

    public int getFoodId() {
        return foodId++;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }


    public Food spawnRandomGrass(){
        Food grass=new Grass();
        grass.setId(getFoodId());
        grass.setValue(getRandomNumber(1,100));
        grass.setMyTransform(new MyTransform(
                getRandomNumber(-100,100),
                getRandomNumber(-100,100),
                getRandomNumber(0,359),1,1
        ));
        return grass;
    }
    public int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }
}
