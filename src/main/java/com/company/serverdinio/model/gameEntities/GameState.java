package com.company.serverdinio.model.gameEntities;

import java.util.ArrayList;
import java.util.List;

public class GameState {
    private List<Dino> dinos=new ArrayList<>();
    private List<Food> foodList=new ArrayList<>();


    public List<Dino> getDinos() {
        return dinos;
    }

    public void setDinos(List<Dino> dinos) {
        this.dinos = dinos;
    }

    public void addDino(Dino dino){
        dinos.add(dino);
    }

    public List<Food> getFoodList() {
        return foodList;
    }

    public void setFoodList(List<Food> foodList) {
        this.foodList = foodList;
    }

    @Override
    public String toString() {
        return "GameState{" +
                "dinos=" + dinos +
                ", foodList=" + foodList +
                '}';
    }
}
