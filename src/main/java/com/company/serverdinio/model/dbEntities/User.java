package com.company.serverdinio.model.dbEntities;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "dinio_user")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    @Column(name="name")
    private String name;

    @Column(name = "device")
    private String device;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }
}





