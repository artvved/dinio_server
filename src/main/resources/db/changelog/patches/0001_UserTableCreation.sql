CREATE TABLE dinio_user (
  id serial PRIMARY KEY,
  name varchar(255) NOT NULL,
  device_id varchar(255) NOT NULL
);