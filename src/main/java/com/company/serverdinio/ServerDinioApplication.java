package com.company.serverdinio;

import com.company.serverdinio.model.gameEntities.Dino;
import com.company.serverdinio.model.gameEntities.HerbivoreDino;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerDinioApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServerDinioApplication.class, args);
    }

}
