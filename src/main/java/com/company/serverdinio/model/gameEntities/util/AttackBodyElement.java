package com.company.serverdinio.model.gameEntities.util;

public class AttackBodyElement extends BodyElement {
    private int damage;

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public String toString() {
        return "AttackBodyElement{" +
                "damage=" + damage +
                ", boxCollider2D=" + myBoxCollider2D +
                '}';
    }
}
