ALTER TABLE dinio_user
ALTER COLUMN name
DROP NOT NULL;

ALTER TABLE dinio_user
ALTER COLUMN name
SET DEFAULT NULL;

ALTER TABLE dinio_user
RENAME COLUMN device_id TO device;